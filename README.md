# 6502A or W65C02 to MOS8501-CPU-Adapter

Adapter for a 6502A or W65C02 to a MOS8501-CPU

6502A:  J1 closed, J2 open  
W65C02: J1 open,   J2 closed

![](Images/Adapter1.jpg)  
![](Images/Adapter2.jpg)
